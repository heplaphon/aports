# Maintainer: Patrycja Rosa <alpine@ptrcnull.me>
pkgname=py3-thefuzz
pkgver=0.20.0
pkgrel=0
pkgdesc="Fuzzy String Matching in Python"
url="https://github.com/seatgeek/thefuzz"
arch="noarch"
license="GPL-2.0-or-later"
depends="py3-levenshtein"
makedepends="py3-gpep517 py3-setuptools py3-wheel"
checkdepends="py3-pytest py3-hypothesis py3-pycodestyle"
subpackages="$pkgname-pyc"
source="https://github.com/seatgeek/thefuzz/archive/refs/tags/$pkgver/py3-thefuzz-$pkgver.tar.gz"
builddir="$srcdir/thefuzz-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	.testenv/bin/python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
5df0b475e709bc6a4d6390511b75315c3e64c9867c072a2da432a4febd56da4eb4caf4671072d1284e7df3b68de90c76282e417763872f92a306bdcd7ed8b7f3  py3-thefuzz-0.20.0.tar.gz
"
