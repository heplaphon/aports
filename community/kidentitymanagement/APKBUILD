# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kidentitymanagement
pkgver=23.08.0
pkgrel=0
pkgdesc="KDE PIM libraries"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kontact.kde.org"
license="LGPL-2.0-or-later"
depends_dev="
	kcodecs-dev
	kcompletion-dev
	kconfig-dev
	kcoreaddons-dev
	kiconthemes-dev
	kio-dev
	kpimtextedit-dev
	ktextwidgets-dev
	kxmlgui-dev
	qt5-qtbase-dev
	"
makedepends="
	$depends_dev
	extra-cmake-modules
	samurai
	"
checkdepends="xvfb-run"
_repo_url="https://invent.kde.org/pim/kidentitymanagement.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kidentitymanagement-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build

	# kpimidentity-signaturetest is broken
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -E "kpimidentity-signaturetest"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
d9fc97db91e1a6783403329bb1b3cf041f682c3e6d51c2331078aa2d3f8397a2d08f42df920260ca1a9eccc5d82166bd366da4b6649555e65aa4c0e3e35a6193  kidentitymanagement-23.08.0.tar.xz
"
