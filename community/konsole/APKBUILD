# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=konsole
pkgver=23.08.0
pkgrel=0
# armhf blocked by qt5-qtdeclarative
arch="all !armhf"
url="https://kde.org/applications/system/konsole"
pkgdesc="KDE's terminal emulator"
license="GPL-2.0-only AND LGPL-2.1-or-later AND Unicode-DFS-2016"
depends="ncurses-terminfo-base"
makedepends="
	extra-cmake-modules
	kbookmarks-dev
	kcompletion-dev
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kcrash-dev
	kdbusaddons-dev
	kdoctools-dev
	kglobalaccel-dev
	kguiaddons-dev
	ki18n-dev
	kiconthemes-dev
	kinit-dev
	kio-dev
	knewstuff-dev
	knotifications-dev
	knotifyconfig-dev
	kparts-dev
	kpty-dev
	kservice-dev
	ktextwidgets-dev
	kwidgetsaddons-dev
	kwindowsystem-dev
	kxmlgui-dev
	qt5-qtbase-dev
	qt5-qtmultimedia-dev
	samurai
	"
checkdepends="mesa-dri-gallium xvfb-run"
_repo_url="https://invent.kde.org/utilities/konsole.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/konsole-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DENABLE_PLUGIN_SSHMANAGER=ON
	cmake --build build
}

check() {
	cd build

	# DBusTest requires running DBus
	local skipped_tests="("
	local tests="
		DBus
		History
		"
	case "$CARCH" in
		armv7)
			tests="$tests Part"
			;;
	esac
	for test in $tests; do
		skipped_tests="$skipped_tests|$test"
	done
	skipped_tests="$skipped_tests)Test"
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -E "$skipped_tests"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
09cab00340d163d8d9e336c851933a68738504c0a77822a3da4b25324c5a499487384f6564da8958d1fc89a50b43da25031fb67c01fd790c655be3e9bb84eb1f  konsole-23.08.0.tar.xz
"
