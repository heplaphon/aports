# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=ktimer
pkgver=23.08.0
pkgrel=0
pkgdesc="A little tool to execute programs after some time"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/applications/utilities/org.kde.ktimer"
license="GPL-2.0-or-later"
makedepends="
	extra-cmake-modules
	kconfig-dev
	kdbusaddons-dev
	kdoctools-dev
	ki18n-dev
	kio-dev
	knotifications-dev
	kwidgetsaddons-dev
	qt5-qtbase-dev
	samurai
	"
_repo_url="https://invent.kde.org/utilities/ktimer.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/ktimer-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DKF_IGNORE_PLATFORM_CHECK=ON # does not have metainfo.yaml
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
d5f900acba5e1275db561800baf9bf0311edfe92ec64317fb37654b4d6a138713cea82be245b6f21df86aff9c0942b075a872a9ba283c32402459f1bc8e18766  ktimer-23.08.0.tar.xz
"
