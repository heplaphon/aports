# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-frameworks
pkgname=kitemmodels
pkgver=5.109.0
pkgrel=0
pkgdesc="Models for Qt Model/View system"
arch="all !armhf" # armhf blocked by extra-cmake-modules
url="https://community.kde.org/Frameworks"
license="LGPL-2.0-only AND LGPL-2.0-or-later"
depends_dev="qt5-qtbase-dev"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	graphviz
	qt5-qtdeclarative-dev
	qt5-qttools-dev
	samurai
	"
checkdepends="xvfb-run"
_repo_url="https://invent.kde.org/frameworks/kitemmodels.git"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kitemmodels-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	# kdescendantsproxymodel_smoketest and kdescendantsproxymodeltest are broken
	# # kselectionproxymodeltest
	xvfb-run ctest --test-dir build --output-on-failure -E "(kselectionproxymodel|kdescendantsproxymodel(_smoke|))test"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
d24fab98390a4fc253fbd39ce925e9147a73715854ff3d84abb794079eaecf67015f932fa88e909519b3dc560324c70606143fd55712741aece3609dbd453e0b  kitemmodels-5.109.0.tar.xz
"
