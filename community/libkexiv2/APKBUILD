# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=libkexiv2
pkgver=23.08.0
pkgrel=0
pkgdesc="Library to manipulate picture metadata"
url="https://www.kde.org/applications/graphics"
arch="all !armhf" # extra-cmake-modules
license="GPL-2.0-or-later"
makedepends="
	exiv2-dev
	extra-cmake-modules
	qt5-qtbase-dev
	samurai
	"
subpackages="$pkgname-dev"
_repo_url="https://invent.kde.org/graphics/libkexiv2.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/libkexiv2-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
c1a40ad25886e25853c7f1b5d8009f1526033bf0cbb65123a01a0c7d6b3cec59592e8b7557eaa62a72314d3f2847b1e0762475188c7ce35dbdeae2a4c146c440  libkexiv2-23.08.0.tar.xz
"
