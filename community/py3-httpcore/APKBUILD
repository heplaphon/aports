# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=py3-httpcore
pkgver=0.17.3
pkgrel=0
pkgdesc="Minimal HTTP client"
url="https://www.encode.io/httpcore/"
license="BSD-3-Clause"
arch="noarch !armhf !ppc64le" # limited by py3-anyio
depends="
	python3
	py3-anyio
	py3-certifi
	py3-h11
	py3-sniffio
	"
makedepends="py3-gpep517 py3-setuptools py3-wheel"
checkdepends="
	py3-h2
	py3-hpack
	py3-hyperframe
	py3-pytest
	py3-pytest-httpbin
	py3-pytest-trio
	py3-socksio
	"
subpackages="$pkgname-pyc"
source="https://github.com/encode/httpcore/archive/$pkgver/py3-httpcore-$pkgver.tar.gz"
builddir="$srcdir/httpcore-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	PYTHONPATH="$PWD/build/lib" pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/httpcore-$pkgver-py3-none-any.whl
}

sha512sums="
41f48d32caa7d7a3456528404392a257927d047f0a1c733e2a68c0fec97c7454c7f8d7d1b6851c5722cfac292dfaa478a4033c656e66d389c81c72dee8e7a7d2  py3-httpcore-0.17.3.tar.gz
"
