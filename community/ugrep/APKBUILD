# Contributor: Francesco Camuffo <dev@fmac.xyz>
# Maintainer: Francesco Camuffo <dev@fmac.xyz>
pkgname=ugrep
pkgver=4.0.2
pkgrel=0
pkgdesc="Ultra fast grep with interactive query UI and fuzzy search"
url="https://github.com/Genivia/ugrep/wiki"
arch="all"
license="BSD-3-Clause"
checkdepends="bash"
makedepends="bzip2-dev lz4-dev pcre2-dev xz-dev zlib-dev zstd-dev"
subpackages="$pkgname-doc"
source="$pkgname-$pkgver.tar.gz::https://github.com/Genivia/ugrep/archive/refs/tags/v$pkgver.tar.gz"

build() {
	CXXFLAGS="$CXXFLAGS -O2 -flto=auto" \
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man
	make
}

check() {
	make test
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="
06259174f2cd90cdc4c80883e2d23954d5ec801eac4e4e70dad02979eb504fc8cd71b80fdd757bf28cb0b906c3ec2a9dc3ee52b7e1999e685cf1dd762e1579e4  ugrep-4.0.2.tar.gz
"
