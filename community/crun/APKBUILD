# Contributor: kohnish <kohnish@gmx.com>
# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=crun
pkgver=1.8.7
pkgrel=0
pkgdesc="Fast and lightweight fully featured OCI runtime and C library for running containers"
url="https://github.com/containers/crun"
license="GPL-2.0-or-later AND LGPL-2.1-or-later"
arch="all"
makedepends="libcap-dev libseccomp-dev yajl-dev argp-standalone python3 go-md2man"
subpackages="$pkgname-doc $pkgname-static"
source="https://github.com/containers/crun/releases/download/$pkgver/crun-$pkgver.tar.xz"

provides="oci-runtime"
provider_priority=100 # highest, default provider

# secfixes:
#   1.4.4-r0:
#     - CVE-2022-27650

build() {
	./configure \
		--prefix=/usr \
		--disable-systemd
	make
}

check() {
	make tests/tests_libcrun_errors.log
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="
74d9f406de40512fa1694f9a744563c7a0eb217da700298da4afea065bd2181d818dd1d25098164eb3dacc47c149a45a57fd79445f40865ffcced383297491a4  crun-1.8.7.tar.xz
"
