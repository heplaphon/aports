# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kontactinterface
pkgver=23.08.0
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kontact.kde.org/"
pkgdesc="Kontact Plugin Interface Library"
license="LGPL-2.0-only OR LGPL-3.0-only"
depends_dev="
	kcoreaddons-dev
	ki18n-dev
	kiconthemes-dev
	kparts-dev
	kwindowsystem-dev
	kxmlgui-dev
	"
makedepends="
	$depends_dev
	extra-cmake-modules
	samurai
	"
_repo_url="https://invent.kde.org/pim/kontactinterface.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kontactinterface-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
dbac8319d7ee085e56f6950ff55138200062907449f1051986312a9073f779f6718619618ef5c0f0fc8486d5087ca1d280c942be3d3b24409fe0579e1708373b  kontactinterface-23.08.0.tar.xz
"
