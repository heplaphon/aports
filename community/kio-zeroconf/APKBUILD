# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kio-zeroconf
pkgver=23.08.0
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://www.kde.org/applications/internet/"
pkgdesc="Network Monitor for DNS-SD services (Zeroconf)"
license="GPL-2.0-or-later AND LGPL-2.0-only AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	kdbusaddons-dev
	kdnssd-dev
	ki18n-dev
	kio-dev
	qt5-qtbase-dev
	samurai
	"
_repo_url="https://invent.kde.org/network/kio-zeroconf.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kio-zeroconf-$pkgver.tar.xz"
subpackages="$pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
2732aa5b2881f19c1a2998d9b289887314dbb6c8e31996a7385a4e79698bc1a285ba2e6f8f060aa21b8f5c39c80f00c6897cd6278c17e32b4ff88351bb1b6f6a  kio-zeroconf-23.08.0.tar.xz
"
