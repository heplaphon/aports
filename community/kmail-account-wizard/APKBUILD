# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kmail-account-wizard
pkgver=23.08.0
pkgrel=0
# armhf blocked by extra-cmake-modules
# ppc64le, s390x and riscv64 blocked by qt5-qtwebengine -> kmailtransport
arch="all !armhf !ppc64le !s390x !riscv64"
url="https://kontact.kde.org/"
pkgdesc="KMail account wizard"
license="GPL-2.0-or-later AND LGPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	akonadi-dev
	akonadi-mime-dev
	extra-cmake-modules
	gpgme-dev
	kcmutils-dev
	kcodecs-dev
	kconfig-dev
	kcrash-dev
	kdbusaddons-dev
	kdoctools-dev
	kidentitymanagement-dev
	kimap-dev
	kldap-dev
	kmailtransport-dev
	knewstuff-dev
	knotifications-dev
	knotifyconfig-dev
	kross-dev
	kservice-dev
	ktexteditor-dev
	kwallet-dev
	libkdepim-dev
	libkleo-dev
	pimcommon-dev
	qt5-qtbase-dev
	samurai
	shared-mime-info
	"
_repo_url="https://invent.kde.org/pim/kmail-account-wizard.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kmail-account-wizard-$pkgver.tar.xz"
subpackages="$pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
6381f10b987b242954454014a4cb61112c139032368d98d3895adfc85e67035843e8f535475c4cd7e461c73a906271b42a72f6a410ccdb76adecd4422333edd1  kmail-account-wizard-23.08.0.tar.xz
"
