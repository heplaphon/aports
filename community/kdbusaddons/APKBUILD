# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-frameworks
pkgname=kdbusaddons
pkgver=5.109.0
pkgrel=0
pkgdesc="Addons to QtDBus"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-only OR LGPL-3.0-only"
makedepends="
	doxygen
	extra-cmake-modules
	graphviz
	qt5-qttools-dev
	shared-mime-info
	samurai
	"
checkdepends="dbus"
_repo_url="https://invent.kde.org/frameworks/kdbusaddons.git"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kdbusaddons-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	dbus-run-session -- ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
2efb1ea210e5ba1f8c99cf924ab14643b51086e8fcb62f7b24bd3b4fdfe7f6ba6a76e36f880a62836e69990334ce1f003e9587e6019858905794141f3b0c573e  kdbusaddons-5.109.0.tar.xz
"
