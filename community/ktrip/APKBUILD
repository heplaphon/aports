# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=ktrip
pkgver=23.08.0
pkgrel=0
pkgdesc="A public transport assistant"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://invent.kde.org/utilities/ktrip"
license="GPL-2.0-only OR GPL-3.0-only"
depends="
	kde-icons
	kirigami-addons
	kirigami2
	"
makedepends="
	extra-cmake-modules
	kconfig-dev
	kcontacts-dev
	ki18n-dev
	kitemmodels-dev
	kpublictransport-dev
	qqc2-desktop-style-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	qt5-qtquickcontrols2-dev
	samurai
	"
_repo_url="https://invent.kde.org/utilities/ktrip.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/ktrip-$pkgver.tar.xz"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DKF_IGNORE_PLATFORM_CHECK=ON
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
1de67baeb54df6718c11c180d9cb6c472cff2f77e1be852708d213e44b00273ac0aabdc23e521cd324fbb33a9ef6825db3792d9e8fab9135b63f1a9ec8d05b14  ktrip-23.08.0.tar.xz
"
