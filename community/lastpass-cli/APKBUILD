# Contributor: Francesco Colista <fcolista@alpinelinux.org>
# Maintainer: Francesco Colista <fcolista@alpinelinux.org>
pkgname=lastpass-cli
pkgver=1.3.5
pkgrel=0
pkgdesc="LastPass command line interface tool"
url="https://lastpass.com"
arch="all"
license="GPL-2.0-or-later"
makedepends="
	asciidoc
	cmake
	curl-dev
	libxml2-dev
	openssl-dev>3
	samurai
	"
checkdepends="bash"
subpackages="
	$pkgname-doc
	$pkgname-zsh-completion
	$pkgname-bash-completion
	$pkgname-fish-completion
	"
source="$pkgname-$pkgver.tar.gz::https://github.com/lastpass/lastpass-cli/archive/v$pkgver.tar.gz
	e3311cebdb29a3267843cf656a32f01c5062897e.patch
	"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_MANDIR=/usr/share/man
	cmake --build build
	if want_check; then
		cmake --build build --target lpass-test
	fi
}

check() {
	ctest --test-dir build --output-on-failure -j1
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install install-doc

	install -D -m644 "$builddir"/contrib/lpass_zsh_completion \
		"$pkgdir"/usr/share/zsh/site-functions/_$pkgname

	install -D -m644 "$builddir"/contrib/lpass_bash_completion \
		"$pkgdir"/usr/share/bash-completion/completions/$pkgname.bash

	install -D -m644 "$builddir"/contrib/completions-lpass.fish \
		"$pkgdir"/usr/share/fish/vendor_completions.d/$pkgname.fish
}

sha512sums="
baa0b35a7293a2df804ed399b0d56257b19e574242b848c7b6c2949eef3942b800123cbc7c61318916139d2b03061c55793f87bcf17e5040107b750e3c33b3e2  lastpass-cli-1.3.5.tar.gz
0f624e6b83d7d193089b2b13d03f261dc8e9df88cafa9b295ad55a6242ef5cca65c2d8ecb3d7330034a1b3d1bfef4a76d5d07491439f0360ac285cba4f050de5  e3311cebdb29a3267843cf656a32f01c5062897e.patch
"
