# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=krecorder
pkgver=23.08.0
pkgrel=0
pkgdesc="Audio recorder for Plasma Mobile (and other platforms)"
url="https://invent.kde.org/plasma-mobile/krecorder"
arch="all !armhf" # armhf blocked by extra-cmake-modules
license="GPL-3.0-or-later"
depends="
	kirigami-addons
	kirigami2
	"
makedepends="
	extra-cmake-modules
	ki18n-dev
	kirigami-addons-dev
	kirigami2-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	qt5-qtmultimedia-dev
	qt5-qtquickcontrols2-dev
	samurai
	"
_repo_url="https://invent.kde.org/utilities/krecorder.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/krecorder-$pkgver.tar.xz"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
37c3a772b28c759d7d9abed23c2c4c7e5171574f88d99dc98f9bb4ac616247144f90c5a8aa0c5dee8496e0893c9fd29dcd8c29ea6d99da280c84810a4bfdccba  krecorder-23.08.0.tar.xz
"
