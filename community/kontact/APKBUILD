# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kontact
pkgver=23.08.0
pkgrel=0
# armhf blocked by extra-cmake-modules
# ppc64le, s390x and riscv64 blocked by qt5-qtwebengine
# riscv64 disabled due to missing rust in recursive dependency
arch="all !armhf !ppc64le !s390x !riscv64"
url="https://kontact.kde.org/"
pkgdesc="Container application to unify several major PIM applications within one application"
license="GPL-2.0-or-later"
makedepends="
	akonadi-dev
	extra-cmake-modules
	grantleetheme-dev
	kcmutils-dev
	kcrash-dev
	kdbusaddons-dev
	kdoctools-dev
	kiconthemes-dev
	kontactinterface-dev
	kpimtextedit-dev
	kwindowsystem-dev
	libkdepim-dev
	pimcommon-dev
	qt5-qtbase-dev
	qt5-qtwebengine-dev
	samurai
	"
_repo_url="https://invent.kde.org/pim/kontact.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kontact-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
5b25f79bb90c3dc29430a8b5c49340fc010df21c1d49e110d5eedcf5395ad7ea3e6a40104e9cd4d9f0d261f9c98bd8d33bf342eeef0492381ad436410e90b91a  kontact-23.08.0.tar.xz
"
